# Telebot

This is a project boilerplate to create an amazing Telegram Bot with node-telegram-bot-api and TypeScript!

## Up and Running

This project counts with a few scripts to help your life.

First, you want to install all dependencies with

```
yarn install
```

For security purposes, your bot's token will be stored in a file called `.env`, which is not tracked by Git.
There's an example file called `.env.sample`.
Generate `.env` with:

```
cp .env.sample .env
```

and replace the placeholder with your real token.

> You might have to talk with your team to discover your token.

Finally, you can run the development server, which will auto reload when you change some file, with:

```
yarn dev
```

Then simply open `index.ts` and make something awesome!

---

As your development flows, `index.ts` tends to get messy. In that case, it's advised you create a module -- but don't worry, there's a script to help you!

Just run
```
bin/newModule <module name>
```

It will scaffold a new file `src/<module name>.ts` and alter `src/index.ts` to require it.

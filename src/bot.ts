import { config } from 'dotenv';
import TelegramBot from 'node-telegram-bot-api';

config();

const token: string = (process.env.TOKEN as string);

const Bot: TelegramBot = new TelegramBot(token, { polling: true });

export default Bot;
